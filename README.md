# Aloy - Production Ready Cypress based End to End Test Framework.

![](aloydemo.gif)

> Owner : Balaji Santhana Krishnan

> LinkedIn : https://www.linkedin.com/in/balaskrishnan/

## Table of Contents

1. [Why should I use Cypress instead of Selenium?](#why-should-i-use-cypress-instead-of-selenium)

2. [Why Aloy? Why not use Cypress Directly?](#why-aloy-why-not-use-cypress-directly)

3. [How does Aloy strengthen Cypress?](#how-does-aloy-strengthen-cypress)

4. [Getting Started](#getting-started)

5. [Running Tests](#running-tests)

6. [Reporting](#test-reports)

7. [Code Examples](#code-examples)

8. [Understanding Cypress.json for ALOY](#understanding-custom-configs-for-ALOY)

9. [Official Cypress Documentation](#cypress-official-site)

## Why should I use Cypress instead of Selenium?

This was my first question coming from a Selenium Background. I am sure it would have popped in your head as well. In the below document, I set the background on the problems of Selenium and how Cypress solves it. To be fair, I have also given the limitations of Cypress as a quick read.

> https://bit.ly/33tFf8r

## Why Aloy? Why not use Cypress Directly?

Cypress is a framework in itself, however, vanilla cypress is not production ready. In ideal production ready test frameworks, we add multiple connectors/integration layers for specific requirments. Aloy aims to strenghten Cypress out of the box for commonly used test framework requirments. Aloy has enough production ready features to start writing Tests directly.

## How does Aloy strengthen Cypress?

1. Support for Accessiblity Testing with AXE tool
   - AXE (a popular tool for Automated Accessibility Testing) is integrated with Aloy. 
   [Refer Example](#cypress/integration/accessibility-examples/completepage.spec.js)
   - Out of the box, the axe tool does not print out accessibility violations on command line.
   - **_This has been fixed in Aloy! On executing accessibility tests, you will notice a tabular representation of the violation with impact, description and affected nodes_**

---
2. Reporting
   - Cypress out of the box support for a good test reporter is not great! Mind you, Cypress's reporting via Dashboards is awesome, But its limited for few users and offers better reporting as paid service.
   - The command line reporter "mochawesomereports" cannot club test run results as a single report, so you end up having X test reports for X Test cases in a single run.
   - **_Aloy Fixes this problem! By executing a single command, all these reports are grouped and given to you as a single report_**

---
3. Support for Cucumber(BDD) Tests
   - Cypress does not support BDD(Cucumber) out of the box.
   - **_Aloy has this fixed and integrated. You are ready to start writing BDD style tests out of the box_**
     [Refer Example](#cypress/integration/cukes-examples/AMHomePage.feature)

---
4. Intelligent Code and Property Completion Configured out of the box!
   - With Cypress, it needs to be configured manually.
   - **_Aloy comes with Integrated Intellisense for Cypress Commands and Properties_**

& More integrations Planned (WIP)...

## Getting Started

1. Ensure that Node.js 8+ is installed
   https://nodejs.org/en/download/

2. Clone this repo locally and install cypress via NPM

```
    $ git clone git@gitlab.com:gitBsk/aloy.git
    $ cd aloy
    $ npm install
```

For other ways of installing Cypress https://docs.cypress.io/guides/getting-started/installing-cypress.html#Installing

3. Cypress should be NOW be installed under node_modules/.bin/

   All further Cypress commands should be invoked with absolute path.

   **[Optional]**

   Create an permanent Alias as below in the shell that you use, if dont want to type the whole path everytime

```
    $ vi ~/.zshrc [~ is my HOME directory, .zshrc depends on the shell you use]
    $ alias cy="~/aloy/node_modules/.bin/cypress"
    $ source ~/.zshrc
```

## Running Tests

> From root directory, run Cypress via

```
    To run all tests under integration/
    $ cy run OR npm run test

    OR

    To run a specific test
    $ cy run --spec path/to/specFile

    To run a specific test folder
    $ cy run --spec "/Users/bsanthana/aloy/cypress/integration/features-examples/*"

    To open Cypress Runner UI - Preferred for Debuggablity
    $ cy open
```

## Test Reports

Cypress's inbuilt reporting via CLI is very simple. Their UI Dashboard/Reporting is good but it's free for upto 3 users only. Mochawesome can fill this gap. I have integrated mochawesome as default Reporter under cypress.json.

However, Mochawesome Reports generate seperate files for each test. Consolidated Test Reports are not inbuilt which is a problem.

> **_This has been fixed with internal scripts that I have written_**

#### Generate Reports in HTML and JSON

```
1. Run the tests
2. npm run generateReports
    This script will merge all the seperate test files as one JSON and one HTML file.
3. npm run cleanup
```

**_This will remove previous report folders containing JSON and HTML files.
Reports cannot be over written for now. I will work on a fix for timestamping the reports.
Till then cleanup(line 3.) has to be run before generating the next report._**

## Code Examples

All code examples have enough documentation as comments. Please read them.

1. Cucumber BDD Test - cypress/integration/cukes-examples

   > Aloy can run files of extension .feature & \*.spec.js. If you want aloy to recognize other formats as test files, append "testFiles" config in cypress.json

2. UI Tests - cypress/integration/features-examples

   > - autowait.spec.js => Shows Cypress's Auto wait and Retry feature. Read about it.
   > - datadriventest.spec.js => Example on how to read test data from JSON file
   > - assertion.spec.js => Element Assertion Example
   > - forcedclick.spec.js => How Cypress handles flaky behaviour

3. API Tests - cypress/integration/services-examples
   > A simple API test written with Cypress.

## Understanding Custom Configs for ALOY

Cypress.json is place where all default configs of cypress are overridden for our specific requirment. I will explain in detail on what are the configs, I have overridden and why?
**_All the below configs can be modified to your needs._**

> #### "testFiles": ["**/*.feature","**/*.spec.js"]
>
> Files ending with .feature(for Cucumber) and .spec.js are considered to be Test Files.
>
> #### "fixturesFolder": "cypress/test-data-dictionary"
>
> Fixtures are where Test Data resides. Have pointed to custom directory.
>
> #### $schema: "https://on.cypress.io/cypress.schema.json"
>
> Pointing to Cypress schema for Code Completion and Intellisense to work. **_Try not to change this!_**
>
> #### "reporter": "mochawesome"
>
> Have used mochawesome reports for Cy run as default reporter. The other options below this pertains to mochawesome reporter.

> #### "retries":
>
> Retires on Test Failure. Cy run mode and Cy open mode is 2.

## Cypress Official Site

https://docs.cypress.io
