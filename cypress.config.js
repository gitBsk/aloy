const { defineConfig } = require('cypress')

module.exports = defineConfig({
  fixturesFolder: 'cypress/test-data-dictionary',
  reporter: 'mochawesome',
  reporterOptions: {
    overwrite: 'false',
    charts: 'true',
    html: 'true',
    json: 'true',
    reportDir: 'cypress/reports/mochawesome-report',
  },
  retries: {
    runMode: 2,
    openMode: 2,
  },
  e2e: {
    // We've imported your old cypress plugins here.
    // You may want to clean this up later by importing these.
    setupNodeEvents(on, config) {
      return require('./cypress/plugins/index.js')(on, config)
    },
  },
})
