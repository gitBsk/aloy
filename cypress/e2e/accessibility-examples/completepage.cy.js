describe("Test Accessibility for the Whole TestRail.com Landing Page", function () //Test Suite
{
  it("Find issues in TestRail.com", function () //Test Case
  {
    // Visit the home page
    cy.visit("https://www.gurock.com/testrail/");

    // Command to inject Axe;
    // Please make sure this is done only after the page is loaded (i.e, cy.visit)
    // Can also be used in Before Hook to make sure it runs before each test.
    cy.injectAxe();

    // This command will make the test fail when it uncovers accessibility issues.
    // Runnig accessibility checks
    //This is custom command, please see /aloy/cypress/support/commands.js
    // NOTE: THIS TEST WILL THROW 5 VIOLOATIONS!!
    cy.checkAccessibility();
    // NOTE: THIS TEST WILL THROW 5 VIOLOATIONS!!
  });
});
