describe("Test Accessibility for a specific section of TestRail.com", function () //Test Suite
{
  it("Find specific section issues in TestRail.com", function () //Test Case
  {
    // Visit the home page
    cy.visit("https://www.gurock.com/testrail/");

    // Command to inject Axe;
    // Please make sure this is done only after the page is loaded (i.e, cy.visit)
    // Can also be used in Before Hook to make sure it runs before each test.
    cy.injectAxe();

    // This command will make the test fail when it uncovers accessibility issues.
    // Runnig accessibility checks
    //This is custom command, please see /aloy/cypress/support/commands.js
    // For checking specific elements, checkAccessbility can be chained once you retrieve the element
    cy.get(".et_pb_code_inner > img").checkAccessibility();

    // THE TEST NEEDS TO BE UPDATED - TESTRAIL UI HAS CHANGED!!!
  });
});
