import { Given, Then } from 'cypress-cucumber-preprocessor/steps';

Given(/^I am at Altimetrik's Home Page$/, () => {
  
   cy.visit('https://www.altimetrik.com/playground/') 
   
});


Then(/^I should see 4 header tags$/, () => {
     
  cy.get('h3').eq(0).should('contain', 'People.')
  cy.get('h3').eq(1).should('contain', 'Platform.')
  cy.get('h3').eq(2).should('contain', 'Places.')
  cy.get('h3').eq(3).should('contain', 'Process.')

});