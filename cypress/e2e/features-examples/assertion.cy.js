describe('Test Playground Page of Altimetrik.com', function() //Test Suite
{
    it('Assert the Title of the Page', function() //Test Case
    {
        
         
        // every command is invoked by calling cy. similar to driver. in Selenium
        cy.visit('https://www.altimetrik.com/playground') 
        
        //Assertion using .should
        cy.title().should('eq', 'Home - Altimetrik Digital Transformation Catalyst | Home')
        
    })
})