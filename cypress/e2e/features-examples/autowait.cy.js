describe('Automatic Waits Demo', function() //Test Suite
{
    it('Auto Wait feature', function() //Test Case
    {
        /* Scenario
        1. Go to Altimetrik Website/Playground
        2. Verify h3's of the page are loaded with a delay of one second each.
        3. Automate this scenario without writing explicit waits.
        
        In Selenium, we need to achieve this with explicit waits.
        WebDriverWait wait = new WebDriverWait(WebDriverRefrence,10);
        WebElement elementToWaitFor;
        elementToWaitFor= wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(“idoffelementToWaitFor")));


        Problems
        =========
        - Doing so, leads to Flaky behaviour [DOM may not be in ready state at each run] 
          with inconsistent results over multiple runs.

        - Code becomes Messy. More the elements, the messier the code gets.


        Cypress has Auto Wait Feature. No more messy code and explicit waits
        https://docs.cypress.io/guides/core-concepts/introduction-to-cypress.html#Cypress-is-Like-jQuery
        ==========

        See code below

        */
         
        // every command is invoked by calling cy. similar to driver. in Selenium
        cy.visit('https://www.altimetrik.com/playground/') 
        
        //Clean code with no waits with Cypress
        
        cy.get('h3').eq(0).should('contain', 'People.')
        cy.get('h3').eq(1).should('contain', 'Platform.')
        cy.get('h3').eq(2).should('contain', 'Places.')
        cy.get('h3').eq(3).should('contain', 'Process.')         

        /* Go ahead, run this test multiple times. You will not notice flaky behavior seen
         in Selenium. Since Waits are handled intelligently in CyPress, Flakiness is no longer
         a problem. Also, no random time out errors as seen as in Selenium*/

        
    })
})