describe('Tests that take test data from testDataDirectory', function()
{

    /*
    In Aloy, all test data can be specified under cypress/testdatadirectory
    For our example, we have our test data coming from cheapair.json
    */

    // Setup to Load Data from testDataDirectory
     // runs once before all tests in the block
    before(function() {
   
    // .fixuture => calls to test-data-dictionary
    // .then is to resolve a promise(JS basics)
    // also make the contents of file to be accessed Globally
    cy.fixture('cheapair').then(function(data)
    {
        this.data=data //for global access
    })
    })
    
    it('Data Driven Test', function()
    {
       cy.visit('https://www.cheapair.com/')

       // We will be populating the From and To Field from cheapair.json
       cy.get('#from1').click().type(this.data.From)
       cy.get('#to1').click().type(this.data.To) 
    
    })
})