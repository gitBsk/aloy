describe('Handling elements which are covered by another element', function()
{

    /*
    In Selenium, if an element covers another element, the test case would fail.
    Cypress automatically closes alerts, windows or elements that are covering an element,
    no more flaky behavior which has affected Selenium for years now. 
    */
    it('Forced Click - Cheap Air', function()
    {
        cy.visit('https://www.cheapair.com/')
        
        cy.get('#to1').click().type('n') //Typing 'n' to delibrately cover the "chicago tile"

        cy.wait(5000) //Introducing the wait forcefully to show how an element is covering another element  

        // cy.get('.CHI > .tile').click()
        cy.get('.CHI > .tile').click({force:true}) 
        //the previous element is closed by cypress intelligently, cy clicks on the Chicago tile
        

    })
})